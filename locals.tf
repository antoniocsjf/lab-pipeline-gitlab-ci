locals {
  common_tags = {
    owner      = "devopsudemy"
    managed-by = "terraform"
  }
}
